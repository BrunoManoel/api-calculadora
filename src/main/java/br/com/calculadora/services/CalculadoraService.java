package br.com.calculadora.services;

import br.com.calculadora.DTOs.RespostaDTO;
import br.com.calculadora.models.Calculadora;
import org.springframework.stereotype.Service;

@Service
public class CalculadoraService {


    public RespostaDTO somar(Calculadora calculadora){
        int resultado = 0;
        for (Integer numero : calculadora.getNumeros()){
            resultado = resultado+numero;
        }

        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return respostaDTO;
    }
}